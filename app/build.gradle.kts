plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.jetbrains.kotlin.android)
    id("kotlin-kapt")

}

android {
    namespace = "com.example.projetofinal"
    compileSdk = 34


    defaultConfig {
        applicationId = "com.example.projetofinal"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    dataBinding {
        enable = true
    }
    buildFeatures{
        dataBinding = true
        viewBinding = true
    }
}

dependencies {
    implementation(libs.androidx.databinding.runtime)
    implementation(libs.core.ktx)
    val roomVersion = "2.6.1"
    val lifecycle_version = "2.7.0"

    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.7.0")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.7.0")

    implementation("androidx.room:room-runtime:2.6.1")
    annotationProcessor("androidx.room:room-compiler:2.6.1")

    kapt("androidx.room:room-compiler:2.6.1")

    implementation("androidx.room:room-ktx:2.6.1")
    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.material)
    implementation(libs.androidx.activity)
    implementation(libs.androidx.constraintlayout)
    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.junit)
    androidTestImplementation(libs.androidx.espresso.core)

    implementation("com.android.support:cardview-v7:26.1.0")
    implementation("com.squareup.picasso:picasso:2.5.2")

    implementation("com.squareup.retrofit2:retrofit:2.3.0")
    implementation("com.squareup.retrofit2:converter-gson:2.3.0")
    implementation("com.jakewharton.picasso:picasso2-okhttp3-downloader:1.1.0")

    implementation("com.squareup.picasso:picasso:2.4.0")

    testImplementation("junit:junit:4.12")
    testImplementation("org.mockito:mockito-core:2.4.0")
    testImplementation("org.powermock:powermock-core:1.7.0RC2")
    testImplementation("org.powermock:powermock-module-junit4:1.7.0RC2")
    testImplementation("org.powermock:powermock-api-mockito2:1.7.0RC2")
    testImplementation("io.mockk:mockk:1.9.3")
}