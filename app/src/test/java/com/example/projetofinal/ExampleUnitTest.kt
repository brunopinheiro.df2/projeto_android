package com.example.projetofinal

import com.example.projetofinal.model.Singleton
import com.example.projetofinal.model.Usuario
import com.example.projetofinal.model.UsuarioDao
import com.example.projetofinal.view.Login
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.powermock.api.mockito.PowerMockito.mock
import org.powermock.core.classloader.annotations.PrepareForTest


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@PrepareForTest(UsuarioDao::class)
class ExampleUnitTest {
    lateinit var login: Login
    lateinit var listUsers: List<Usuario>

    @Mock
    lateinit var singleton: Singleton

    @Test
    fun testeLogin() {
        val repositoryMock = mock(UsuarioDao::class.java)

        var result = login.logar("teste","teste")
        Assert.assertTrue(result.size == 0)
    }

    @Before
    fun setUp() {
        login = Login()
    }

}