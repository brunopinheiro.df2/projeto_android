package com.example.projetofinal.network

import com.example.projetofinal.model.PokemonDetails
import com.example.projetofinal.model.PokemonG
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Url

interface PokemonApi {
    @GET("v2/pokemon")
    fun list(): Call<PokemonG>

    @GET
    fun get(@Url url: String): Call<PokemonDetails>

    @GET("v2/pokemon/{name}")
    fun getByName(@Path("name") name: String): Call<PokemonDetails>

    @GET("v2/pokemon/?limit=20000")
    fun listAll(): Call<PokemonG>
}