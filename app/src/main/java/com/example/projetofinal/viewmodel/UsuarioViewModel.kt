package com.example.projetofinal.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.projetofinal.model.Singleton
import com.example.projetofinal.model.Usuario

class UsuarioViewModel: ViewModel() {
    var usuariosLiveData = MutableLiveData<List<Usuario>>();

    fun add(usuario: Usuario){
        Singleton.add(usuario)
        usuariosLiveData.value = Singleton.usuarios
    }

    fun delete(usuario: Usuario){
        Singleton.delete(usuario);
        usuariosLiveData.value = Singleton.usuarios
    }

    fun update(usuario: Usuario){
        Singleton.update(usuario)
        usuariosLiveData.value = Singleton.usuarios
    }

    fun refresh(){
        usuariosLiveData.value = Singleton.usuarios
    }
}