package com.example.myrecyclerview.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.projetofinal.viewmodel.UsuarioViewModel
import java.lang.IllegalArgumentException

class MainViewModelFactory :ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(UsuarioViewModel::class.java)){
            UsuarioViewModel() as T
        }else{
            throw IllegalArgumentException("ViewModel not found")
        }
    }
}