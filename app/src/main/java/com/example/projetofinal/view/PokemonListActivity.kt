package com.example.projetofinal.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.projetofinal.R
import com.example.projetofinal.databinding.ActivityPokemonListBinding
import com.example.projetofinal.model.Pokemon
import com.example.projetofinal.model.PokemonDetails
import com.example.projetofinal.model.PokemonG
import com.example.projetofinal.model.Singleton
import com.example.projetofinal.network.PokemonClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PokemonListActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPokemonListBinding
    var pokemonsListData: ArrayList<Pokemon> = ArrayList()
    var thisact = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ActivityPokemonListBinding>(this, R.layout.activity_pokemon_list)

        val call = PokemonClient.pokemonService.list()
        var listPokemonsAll = Singleton.allPokemonsList.map { it.name }
        var adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, listPokemonsAll)
        binding.editTextText.setAdapter(adapter)

        binding.button.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    binding.editTextText.text
                    val call2 = PokemonClient.pokemonService.getByName(binding.editTextText.text.toString())
                    call2.enqueue(object : Callback<PokemonDetails> {
                        override fun onResponse(call: Call<PokemonDetails>, response: Response<PokemonDetails>) {
                            if (response.isSuccessful) {
                                var pokemon = response?.body()!!
                                Singleton.pokemonDetailsSelected = pokemon
                                val intent = Intent(this@PokemonListActivity, PokemonDetailsActivity::class.java)
                                startActivity(intent)
                            } else {
                                Toast.makeText(thisact, "Pokémon não encontrado", Toast.LENGTH_SHORT).show()
                            }
                        }

                        override fun onFailure(call: Call<PokemonDetails>, t: Throwable) {
                            TODO("Not yet implemented")
                        }
                    })
                }
            }
        )

        call.enqueue(object : Callback<PokemonG> {
            override fun onResponse(call: Call<PokemonG>, response: Response<PokemonG>) {
                if (response.isSuccessful) {
                    pokemonsListData = response?.body()?.results!!
                    binding.recyclerviewPokemon.adapter = PokemonListAdapter(pokemonsListData, object : PokemonListAdapter.OnPokemonClickListener {
                        override fun onPokemonClick(view: View, position: Int) {
                                Singleton.pokemonSelected = pokemonsListData[position]
                                val intent = Intent(this@PokemonListActivity, PokemonDetailsActivity::class.java)
                                startActivity(intent)
                            }
                        })
                } else {
                    // Handle error
                }
            }

            override fun onFailure(call: Call<PokemonG>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }
}