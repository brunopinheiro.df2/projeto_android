package com.example.projetofinal.view

import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.projetofinal.databinding.ItemViewBinding
import com.example.projetofinal.model.Usuario
import com.example.projetofinal.model.Singleton

class UsuarioAdapter (val clickListener: OnUsuarioClickListener?): RecyclerView.Adapter<UsuarioAdapter.ViewHolder>(){
    interface OnUsuarioClickListener{
        fun onUsuarioClick(view: View, position: Int)
    }

    inner class ViewHolder(val binding: ItemViewBinding) :RecyclerView.ViewHolder(binding.root){
        fun bind(usuario: Usuario){
            binding.textView1.text = usuario.nome
            binding.root.setOnClickListener {
                clickListener?.onUsuarioClick(it, adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemViewBinding.
        inflate(
            LayoutInflater.from(parent.context),
            parent,
            false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Singleton.usuarios[position].apply {
            holder.bind(this)
        }
    }

    override fun getItemCount() = Singleton.usuarios.size

}