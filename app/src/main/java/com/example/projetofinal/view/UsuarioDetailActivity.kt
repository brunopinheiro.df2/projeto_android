package com.example.projetofinal.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.projetofinal.R
import com.example.projetofinal.databinding.ActivityMainBinding
import com.example.projetofinal.databinding.ActivityUsuarioDetailBinding
import com.example.projetofinal.model.Usuario
import com.example.projetofinal.model.Singleton
import kotlin.math.log

class UsuarioDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.
        setContentView<ActivityUsuarioDetailBinding>(this, R.layout.activity_usuario_detail)

        Singleton.usuarioSelected?.apply {
            binding.nameEditText.setText(nome)
            binding.idadeText.setText(idade.toString())
            binding.loginText.setText(login)
            binding.senhaText.setText(senha)
        }

        binding.saveButton.setOnClickListener {
            val nome = binding.nameEditText.text.toString()
            val idade = binding.idadeText.text.toString()
            val login = binding.loginText.text.toString()
            val senha = binding.senhaText.text.toString()

            if (Singleton.usuarioSelected == null) {
                Singleton.add(Usuario(0, nome, idade.toInt(), login, senha))
            }else {
                Singleton.usuarioSelected?.apply {
                    this.nome = nome
                    this.idade = idade.toInt()
                    this.login = login
                    this.senha = senha

                    Singleton.update(this)

                }
            }

            finish()
        }
    }
}