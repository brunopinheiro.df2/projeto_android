package com.example.projetofinal.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myrecyclerview.viewmodel.MainViewModelFactory
import com.example.projetofinal.R
import com.example.projetofinal.databinding.ActivityUsuariosListBinding
import com.example.projetofinal.model.Singleton
import com.example.projetofinal.viewmodel.UsuarioViewModel
import com.google.android.material.snackbar.Snackbar

class UsuariosList : AppCompatActivity() {
    lateinit var binding: ActivityUsuariosListBinding
    lateinit var viewModel: UsuarioViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView<ActivityUsuariosListBinding>(this, R.layout.activity_usuarios_list)
        Singleton.setContext(this)
        viewModel = MainViewModelFactory().create(UsuarioViewModel::class.java)

        viewModel.usuariosLiveData.observe(this, Observer {
            binding.mainRecyclerView.adapter?.notifyDataSetChanged()
        })

        binding.mainRecyclerView.adapter = UsuarioAdapter(object : UsuarioAdapter.OnUsuarioClickListener {
            override fun onUsuarioClick(view: View, position: Int) {
                Singleton.usuarioSelected = Singleton.usuarios[position]
                val intent = Intent(this@UsuariosList, UsuarioDetailActivity::class.java)
                startActivity(intent)
            }

        })


        binding.mainRecyclerView.layoutManager = LinearLayoutManager(this)

        binding.addItemButton.setOnClickListener {
            val intent = Intent(this, UsuarioDetailActivity::class.java)
            Singleton.usuarioSelected = null
            startActivity(intent)
        }

    }

    override fun onResume() {
        super.onResume()
        viewModel.refresh()
    }

}