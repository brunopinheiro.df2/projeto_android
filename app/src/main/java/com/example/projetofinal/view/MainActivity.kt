package com.example.projetofinal.view

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.projetofinal.R
import com.example.projetofinal.databinding.ActivityMainBinding
import com.example.projetofinal.model.PokemonG
import com.example.projetofinal.model.Singleton
import com.example.projetofinal.network.PokemonClient
import com.example.projetofinal.viewmodel.UsuarioViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    lateinit var viewModel: UsuarioViewModel
    lateinit var mPlayer: MediaPlayer
    var musicPlaying: Boolean = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val call = PokemonClient.pokemonService.listAll()
        call.enqueue(object : Callback<PokemonG> {
            override fun onResponse(call: Call<PokemonG>, response: Response<PokemonG>) {
                if (response.isSuccessful) {
                    Singleton.allPokemonsList = response?.body()?.results!!

                } else {
                    // Handle error
                }
            }

            override fun onFailure(call: Call<PokemonG>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        criarMediaPlayer()

        binding.listUsers.setOnClickListener {
            val intent = Intent(this, UsuariosList::class.java)

            startActivity(intent)
        }

        binding.listpokemon.setOnClickListener {
            val intent = Intent(this, PokemonListActivity::class.java)

            startActivity(intent)
        }

        binding.imageButton.setOnClickListener{
            if(mPlayer.isPlaying) {
                mPlayer.stop()
                binding.imageButton.setImageResource(android.R.drawable.ic_media_play)
            }else {
                mPlayer.prepare()
                mPlayer.start()
                binding.imageButton.setImageResource(android.R.drawable.ic_media_pause)
            }
        }
    }

    public fun criarMediaPlayer(): Boolean
    {
        try{
            mPlayer= MediaPlayer.create(getApplicationContext(), R.raw.pokemon)
            mPlayer.start()

            if(mPlayer.isPlaying) {
                binding.imageButton.setImageResource(android.R.drawable.ic_media_pause)
            }else {
                binding.imageButton.setImageResource(android.R.drawable.ic_media_play)
            }

            return true
        }catch (e: Exception){
            return  false
        }
    }

    fun teste(){
        var sad = "asd"
    }

    override fun onResume() {
        super.onResume()
    }
}