package com.example.projetofinal.view

import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.projetofinal.databinding.ItemViewBinding
import com.example.projetofinal.model.Pokemon
import com.example.projetofinal.model.Usuario
import com.example.projetofinal.model.Singleton

class PokemonListAdapter (var pokemonsList: ArrayList<Pokemon>, var onPokemonClickListener : OnPokemonClickListener): RecyclerView.Adapter<PokemonListAdapter.ViewHolder>(){
    interface OnPokemonClickListener{
        fun onPokemonClick(view: View, position: Int)
    }

    inner class ViewHolder(val binding: ItemViewBinding) :RecyclerView.ViewHolder(binding.root){
        fun bind(pokemonList: ArrayList<Pokemon>, position: Int){
            binding.textView1.text = pokemonList!!.get(position).name

            binding.root.setOnClickListener {
                onPokemonClickListener?.onPokemonClick(it, adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemViewBinding.
        inflate(
            LayoutInflater.from(parent.context),
            parent,
            false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(pokemonsList, position)

    }

    override fun getItemCount(): Int {
        return  pokemonsList!!.size
    }

}