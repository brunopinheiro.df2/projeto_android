package com.example.projetofinal.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.projetofinal.R
import com.example.projetofinal.model.Usuario
import com.example.projetofinal.model.UsuarioDao
import com.example.projetofinal.model.UsuarioDatabase


class LoginActivity() : AppCompatActivity() {
    lateinit var dao: UsuarioDao
    lateinit var login: Login
    var thisCtx = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        login = Login()
        preencherUsuarioDao()

        setContentView(R.layout.activity_login)

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        var button = this.findViewById<Button>(R.id.entrar)

        button.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                if(verificarAdmin()){
                    val intent = Intent(thisCtx, MainActivity::class.java)
                    startActivity(intent)
                }else{
                    var logado = logar()

                    if(logado.size > 0)
                    {
                        val intent = Intent(thisCtx, MainActivity::class.java)
                        startActivity(intent)
                    }else{
                        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
                        Toast.makeText(thisCtx, "Login inválido", Toast.LENGTH_SHORT).show()
                    }
                }
            }})
    }

    fun logar(): List<Usuario>{
        var loginText = findViewById<EditText>(R.id.loginText).getText().toString();
        var senhaTxt = findViewById<EditText>(R.id.senhaText).getText().toString();
        var logado = login.logar(loginText, senhaTxt)
        return logado
    }

    fun verificarAdmin(): Boolean{
        var loginText = findViewById<EditText>(R.id.loginText).getText().toString();
        var senhaTxt = findViewById<EditText>(R.id.senhaText).getText().toString();

        if(loginText == "admin" && senhaTxt == "admpoke")
            return true
        else
            return false
    }

    fun preencherUsuarioDao()
    {
        UsuarioDatabase.getInstance(this)?.apply {
            dao = usuarioDao()
        }
    }

    fun teste(): Boolean{
        return true
    }

}