package com.example.projetofinal.view

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.projetofinal.R
import com.example.projetofinal.databinding.ActivityPokemonDetailsBinding
import com.example.projetofinal.model.PokemonDetails
import com.example.projetofinal.model.Singleton
import com.example.projetofinal.network.PokemonClient
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PokemonDetailsActivity : AppCompatActivity(), GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {
    lateinit var pokemonBack: ImageView
    lateinit var  pokemonFront: ImageView
    lateinit var binding: ActivityPokemonDetailsBinding
    private lateinit var mDetector: GestureDetector
    val thisContext = this

    var  isPokemontBack: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = DataBindingUtil.
        setContentView<ActivityPokemonDetailsBinding>(this, R.layout.activity_pokemon_details)

        mDetector = GestureDetector(this,this)
        mDetector.setOnDoubleTapListener(this)

        pokemonBack = ImageView(this)
        pokemonFront = ImageView(this)

        if(Singleton.pokemonDetailsSelected != null)
        {
            preencherDadosPokemon(Singleton.pokemonDetailsSelected!!)
            Singleton.pokemonDetailsSelected = null
        } else {
            val call = PokemonClient.pokemonService.get(Singleton.pokemonSelected.url)
            call.enqueue(object : Callback<PokemonDetails> {
                override fun onResponse(call: Call<PokemonDetails>, response: Response<PokemonDetails>) {
                    if (response.isSuccessful) {
                        var pokemon = response?.body()!!
                        preencherDadosPokemon(pokemon)


                    } else {
                        // Handle error
                    }
                }

                override fun onFailure(call: Call<PokemonDetails>, t: Throwable) {
                    TODO("Not yet implemented")
                }
            })
        }


    }


    fun List<String>.concat() = this.joinToString("") { it }.takeWhile { it.isDigit() }


    fun preencherDadosPokemon(pokemon: PokemonDetails){
        binding.nome.text = pokemon.name.uppercase()
        binding.altura.text = "Altura: \n" + pokemon.height
        binding.peso.text = "Peso: \n" + pokemon.weight.toString()
        val iterator =  pokemon.abilities
        var habilidades: String = "Habilidades: "
        iterator.forEach {
            habilidades += it.ability.name + ", "
        }

        binding.habilidades.text = habilidades
        pokemonFront = ImageView(thisContext)

        binding.hp.text = "HP: " + pokemon.stats[0].base_stat.toString()
        binding.attack.text = "Attack: " + pokemon.stats[1].base_stat.toString()
        binding.defense.text = "Defense: " + pokemon.stats[2].base_stat.toString()
        binding.specialAttack.text = "Special Attack: " + pokemon.stats[3].base_stat.toString()
        binding.specialDefense.text = "Special Defense: " + pokemon.stats[4].base_stat.toString()
        binding.speed.text = "Speed: " + pokemon.stats[5].base_stat.toString()

        binding.barHp.progress = pokemon.stats[0].base_stat
        binding.barHp.setProgressTintList(definirCorBarraProgresso(pokemon.stats[0].base_stat));

        binding.barAttack.progress = pokemon.stats[1].base_stat
        binding.barAttack.setProgressTintList(definirCorBarraProgresso(pokemon.stats[1].base_stat));

        binding.barDefense.progress = pokemon.stats[2].base_stat
        binding.barDefense.setProgressTintList(definirCorBarraProgresso(pokemon.stats[2].base_stat));

        binding.barSAttack.progress = pokemon.stats[3].base_stat
        binding.barSAttack.setProgressTintList(definirCorBarraProgresso(pokemon.stats[3].base_stat));

        binding.barSDefense.progress = pokemon.stats[4].base_stat
        binding.barSDefense.setProgressTintList(definirCorBarraProgresso(pokemon.stats[4].base_stat));

        binding.barSpeed.progress = pokemon.stats[5].base_stat
        binding.barSpeed.setProgressTintList(definirCorBarraProgresso(pokemon.stats[5].base_stat));

        binding.type.text = "Tipo: \n" + pokemon.types[0].type.name

        Picasso.with(thisContext)
            .load(pokemon.sprites.front_default)
            .into(pokemonFront, object: com.squareup.picasso.Callback {
                override fun onSuccess() {
                    var animSlide = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.slide);
                    binding.imageView.startAnimation(animSlide);
                    binding.imageView.setImageDrawable(pokemonFront.drawable)
                }

                override fun onError() {
                    TODO("Not yet implemented")
                }

            })


        Picasso.with(thisContext)
            .load(pokemon.sprites.back_default)
            .into(pokemonBack)


    }

    fun definirCorBarraProgresso(valor: Int): ColorStateList
    {
        if(valor <= 30)
            return ColorStateList.valueOf(Color.RED)

        if(valor > 30 && valor <= 70)
            return ColorStateList.valueOf(Color.YELLOW)


        return ColorStateList.valueOf(Color.GREEN)

    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        mDetector.onTouchEvent(event)
        // Be sure to call the superclass implementation
        return super.onTouchEvent(event)
    }

    override fun onDown(e: MotionEvent): Boolean {
        return false
    }

    override fun onShowPress(e: MotionEvent) {

    }

    override fun onSingleTapUp(e: MotionEvent): Boolean {
        return false
    }

    override fun onScroll(
        e1: MotionEvent?,
        e2: MotionEvent,
        distanceX: Float,
        distanceY: Float
    ): Boolean {
        return false
    }

    override fun onLongPress(e: MotionEvent) {
        binding.imageView.setImageDrawable(pokemonBack.drawable)

    }

    override fun onFling(
        e1: MotionEvent?,
        e2: MotionEvent,
        velocityX: Float,
        velocityY: Float
    ): Boolean {
        return false
    }

    override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
        return false
    }

    override fun onDoubleTap(e: MotionEvent): Boolean {
        if(!isPokemontBack)
        {
            isPokemontBack = true
            binding.imageView.setImageDrawable(pokemonBack.drawable)
        }else{
            isPokemontBack = false
            binding.imageView.setImageDrawable(pokemonFront.drawable)
        }
        return false
    }

    override fun onDoubleTapEvent(e: MotionEvent): Boolean {


        return true
    }
}