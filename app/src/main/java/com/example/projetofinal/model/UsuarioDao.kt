package com.example.projetofinal.model

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface UsuarioDao {
    @Insert
    fun insertAll(vararg usuario: Usuario)

    @Query("Select * from Usuarios")
    fun getAll(): List<Usuario>

    @Query("Select * from Usuarios where nome = :name")
    fun getByName(name: String): List<Usuario>

    @Update
    fun update(usuario: Usuario): Int

    @Delete
    fun delete(usuario: Usuario): Int

    @Query("Select * from Usuarios where login = :login and senha = :senha")
    fun login(login: String, senha: String): List<Usuario>
}