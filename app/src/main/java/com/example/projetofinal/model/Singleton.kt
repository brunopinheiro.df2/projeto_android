package com.example.projetofinal.model

import android.content.Context

object Singleton {
    var usuarioSelected: Usuario? = null
    lateinit var dao: UsuarioDao
    lateinit var usuarios: List<Usuario>
    //lateinit var pokemons: List<Pokemon>
    lateinit var pokemonSelected: Pokemon
    lateinit var allPokemonsList: ArrayList<Pokemon>
    var pokemonDetailsSelected: PokemonDetails? = null

    fun setContext(context: Context){
        UsuarioDatabase.getInstance(context)?.apply {
            dao = usuarioDao()
            usuarios = dao.getAll()
        }
    }

    fun add(usuario: Usuario){
        dao.insertAll(usuario)
        usuarios = dao.getAll()
    }

    fun update(usuario: Usuario){
        dao.update(usuario)
        usuarios = dao.getAll()
    }

    fun delete(usuario: Usuario){
        dao.delete(usuario)
        usuarios = dao.getAll()
    }





}