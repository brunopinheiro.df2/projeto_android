package com.example.projetofinal.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Usuarios")
data class Usuario (
    @PrimaryKey(autoGenerate = true)
    var id:Int,
    @ColumnInfo(index = true)
    var nome: String,
    var idade: Int,
    var login: String,
    var senha: String
)