package com.example.projetofinal.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Usuario::class], version = 2)
abstract class UsuarioDatabase: RoomDatabase()
{
    abstract fun usuarioDao(): UsuarioDao
    companion object{
        private var INSTANCE: UsuarioDatabase? = null
        fun getInstance(context: Context): UsuarioDatabase?{
            if(INSTANCE == null)
            {
                synchronized(UsuarioDatabase::class){
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        UsuarioDatabase::class.java, "usuario.sqlite"
                    ).allowMainThreadQueries().fallbackToDestructiveMigration().build()
                }
            }

            return INSTANCE
        }

        fun destroyDatabase(){
            INSTANCE = null
        }
    }
}