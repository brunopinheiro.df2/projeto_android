package com.example.projetofinal.model

data class PokemonG (
    val count: Int,
    val next: String,
    val previous: String,
    val results: ArrayList<Pokemon>
)

data class Pokemon (
    val name: String,
    val url: String
)