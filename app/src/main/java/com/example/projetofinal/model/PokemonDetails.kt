package com.example.projetofinal.model

data class PokemonDetails (
    val id: Int,
    val name: String,
    val base_experience: Int,
    val image: String,
    val height: String,
    val weight: String,
    val abilities: List<Abilities>,
    var sprites: Sprite,
    var types: List<Types>,
    var stats: List<Stats>
)

data class Abilities (
    val ability: Ability
)

data class Ability (
    val name: String
)

data class Types (
    val type: Type
)

data class Type (
    val name: String
)

data class Stats (
    val stat: Stat,
    val base_stat: Int
)

data class Stat (
    val name: String
)

data class Sprite (
    val back_default: String,
    val front_default: String

)

